<?php

include ('getParams.php');

$ipAddress = file_get_contents('http://169.254.169.254/latest/meta-data/public-ipv4');
$instanceID = file_get_contents('http://169.254.169.254/latest/meta-data/instance-id');
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Create DB</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<?php echo $domain; ?>/css.css" rel="stylesheet" />
</head>
<body>

<h1>For this to work you must have configured your highly available RDS and configured SSM using thee below</h1>
<br>
<p>Note - each region has its own SSM so this must be ran on an instance within the same region as your setup or have the "AWS_DEFAULT_REGION" set on the CLI prior to running (for example, if using CloudShell)</p>
<br>
<p>
aws ssm put-parameter --name "/sdn/showServerBool" --type "String" --value "true" --description "Show Server Information (T/F)" --overwrite
aws ssm put-parameter --name "/sdn/domain" --type "String" --value "<b>YOUR_CDN_ENDPOINT</b>" --description "CDN Endpoint" --overwrite
aws ssm put-parameter --name "/sdn/s3URL" --type "String" --value "<b>YOUR_S3_ENDPOINT</b>" --description "S3 Endpoint" --overwrite
aws ssm put-parameter --name "/sdn/dbMasterUrl" --type "String" --value "<b>YOUR_MASTERDB_USERNAME</b>" --description "Master DB URL" --overwrite
aws ssm put-parameter --name "/sdn/dbRRUrl" --type "String" --value "<b>YOUR_RRDB_ENDPOINT</b>" --description "Read Replica DB URL" --overwrite
aws ssm put-parameter --name "/sdn/dbName" --type "String" --value "YOUR_SQL_DATABASE_NAME" --description "Database Name" --overwrite
aws ssm put-parameter --name "/sdn/dbUser" --type "String" --value "<b>YOUR_DB_USERNAME</b>" --description "Database User Name" --overwrite
aws ssm put-parameter --name "/sdn/dbPassword" --type "String" --value "<b>YOUR_DB_PASSWORD</b>" --description "Database Password" --overwrite
</p>

<?php
try {
  $conn = new PDO("mysql:host=$db_masterurl;dbname=$db_name", $db_user, $db_password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $sql = "CREATE TABLE tbl_videos (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  video_name VARCHAR(30) NOT NULL,
  video_url VARCHAR(255) NOT NULL,
  image_url VARCHAR(255) NOT NULL,
  video_description VARCHAR(255),
  upload_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  )";

  // use exec() because no results are returned
  $conn->exec($sql);
  echo "Table tbl_videos created successfully";
} catch(PDOException $e) {
  echo $sql . "<br>" . $e->getMessage();
}

$conn = null;
?>

</body>
</html>