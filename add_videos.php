<?php

include ('getParams.php');

$ipAddress = file_get_contents('http://169.254.169.254/latest/meta-data/public-ipv4');
$instanceID = file_get_contents('http://169.254.169.254/latest/meta-data/instance-id');
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Create DB</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<?php echo $domain; ?>/css.css" rel="stylesheet" />
</head>
<body>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST"){
        $video_name = filter_var($_POST["video_name"], FILTER_SANITIZE_STRING);
        $video_url = filter_var($_POST["video_url"], FILTER_SANITIZE_STRING);
        $image_url = filter_var($_POST["image_url"], FILTER_SANITIZE_STRING);
        $video_description = filter_var($_POST["video_description"], FILTER_SANITIZE_STRING);
        
        echo "" . $video_name . " is being added <br><br>". $video_name;

        if (empty($video_name)){
                die("Please enter video name");
        }
        if (empty($video_url)){
                die("Please enter video name");
        }

        if (empty($image_url)){
                die("Please enter thumbnail name");
        }

        if (empty($video_description)){
                $video_description = "No description given";
	}

        $mysqli = new mysqli($db_masterurl, $db_user, $db_password, $db_name);
        if ($mysqli->connect_error) {
                die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
        }

        $statement = $mysqli->prepare("INSERT INTO tbl_videos (video_name, video_url, image_url, video_description, upload_date) VALUES(?, ?, ?, ?, ?)"); 
        $statement->bind_param('sssss', $video_name, $video_url, $image_url, $video_description, date("Y-m-d H:i:s")); 

        if($statement->execute()){
                echo "** " . $video_name . " has been added! **";
        }else{
                echo $mysqli->error; 
        }
}
?>
