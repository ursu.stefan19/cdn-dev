<?php

include ('getParams.php');

$ipAddress = file_get_contents('http://169.254.169.254/latest/meta-data/public-ipv4');
$instanceID = file_get_contents('http://169.254.169.254/latest/meta-data/instance-id');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CDN Demo (LOCAL)</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css.css" rel="stylesheet" />
<link href="https://vjs.zencdn.net/7.11.4/video-js.css" rel="stylesheet" />
</head>
<body>
<h1>Welcome to the CDN Demo page running on <?php echo gethostname(); ?></h1>
<br>
<p>Note - For this to work the instance must have access to a suitable role and have available SSM configuration!</p>
<br>
<?php
$mysqli = new mysqli($db_rrurl, $db_user, $db_password, $db_name);
if ($mysqli->connect_error) {die("Connection failed: " . $mysqli->connect_error);}
$sql = "SELECT video_name, video_url, image_url, video_description, upload_date FROM tbl_videos";
        $result = $mysqli->query($sql);
        
        if ($result->num_rows > 0) {
          echo "<br><br>";
          echo "<h2>Video Demo</h2>";
          echo "<table border=\"0\" style=\"border-collapse: collapse; width: 100%;\">";
          echo "<tbody>";

        // output data of each row
        while($row = $result->fetch_assoc()) {
          echo "<tr>";
          echo "<td>";

          echo "    <video";
          echo "    id=\"my-video" . $row["id"] . "\"";
          echo "    class=\"video-js\"";
          echo "    controls";
          echo "    preload=\"auto\"";
          echo "    width=\"640\"";
          echo "    height=\"264\"";
          echo "    poster=\"" . $row["image_url"] . "\"";
          echo "    data-setup=\"{}\"";
          echo "  >";
          echo "    <source src=\"" . $row["video_url"] . "\" type=\"video/mp4\" />";
          echo "  </video>";

          echo "</td>";
          echo "<td>";
          echo "<p><strong>" . $row["video_name"] . "</strong> (Upload date: " . $row["upload_date"] . ")</p>";
          echo "<p>" . $row["video_description"] . "</p>";
          echo "</td>";
          echo "</tr>";
}
} else {
echo "No videos found";
}
$mysqli->close();
?>
</tbody>
</table>
</body>
</html>